export enum Scholarships {
    Normal = 1000,
    increased = 1500,
}

export enum PrepLevels {
    Min = 70,
    Middle = 80,
    Good = 90,
}

export enum TeachersSallaryLevels {
    Min = 1500,
    Middle = 2000,
    Good = 2500,
}

export enum Position {
    Student = 'Student',
    Teacher = 'Teacher',
    Librarian = 'Librarian',
    HeadOfDepartment = 'HeadOfDepartment',
    Dean = 'Dean',
}

export enum SubjectsList {
    Chemistry = 'Chemistry',
    Math = 'Math',
    Literature = 'Literature',
}

