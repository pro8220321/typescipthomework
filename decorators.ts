export function studentDecorator(target: any) {
    let origConstructor = target;
    let newConstructor: any = function(name:any,studInfo:any,scholarship:any){
        if(typeof name !== 'string'){
            throw new Error("Name must to be string");
        }
        return new origConstructor(name,studInfo,scholarship);
    }
    newConstructor.prototype = origConstructor.prototype;
    return newConstructor;
}

export function creatingMethodLog(target: Object, propertyKey: string | symbol,parameterIndex: number) {
    console.log(`*creating method ' + ${String(propertyKey)}*`);
}

export function defaultValue(defaultValue: any) {
    return function(target: any, propertyKey: string) {
      target[propertyKey] = defaultValue;
    };
}

export function logSalarySetter(target: any, propertyKey: string, descriptor: PropertyDescriptor) {
    const originalSetter = descriptor.set;
  
    descriptor.set = function (value: string) {
      originalSetter?.call(this, value);
      console.log(`*start to set ${propertyKey}*`);
    };
  }

export function getSalary(salaryPatern:string) {
    return function(target: any, propertyKey: string, descriptor: PropertyDescriptor) {
      const originalGetter = descriptor.get;
  
      descriptor.get = function() {
        const originalValue = originalGetter?.call(this);
        const moderndValue = `${salaryPatern}${originalValue}`;
  
        return moderndValue;
      };
    };
  }
  
export function logUniversityMethod(message: string) {
    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
      const originalMethod = descriptor.value;
  
      descriptor.value = function (...args: any[]) {
        console.log('------------------');
        console.log(message);
        return originalMethod.apply(this, args);
      };
  
      return descriptor;
    };
  }
