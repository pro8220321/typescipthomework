// Enums
import { Scholarships,PrepLevels,TeachersSallaryLevels,Position,SubjectsList } from "./enums";

// Decorators
import {studentDecorator, creatingMethodLog, defaultValue,logSalarySetter,getSalary,logUniversityMethod}  from "./decorators";

// Interfaces
interface StudentBase {
    studInfo: studentsInfo[];
    scholarship: ScholarshipLevels;
}

interface StudentMethods {
    analysisScores(librarian: Librarian,teachers: Teacher[]): void;
}
 
// Custom Types
type ScholarshipLevels = Scholarships | false;

// Utility Types
type ReadonlySalary = Readonly<TeachersSallaryLevels>;

// Regular Types
type employeesType = {
    teachers: Teacher[],
    librarian: Librarian,
    HeadOfDepartment: HeadOfDepartment,
    Dean: Dean
};

type studentsInfo = {
    subject: SubjectsList,
    preparationLevel: number;
    score: number;
}

// Abstract Class
abstract class UniversityEmployee {
    private salary: number;
    constructor(public name: string, public position: Position){
        this.name = name;
        this.position = position;
        this.salary = 0;
    }
    
    @logSalarySetter
    set Salary(salary: TeachersSallaryLevels){
        this.salary = salary;
    }

 
    @getSalary(`Some random university employee: at the end of the mounth my salary = `)
    get getSalary(): ReadonlySalary{
        return this.salary;
    }
}

// Class Inheritance
class Teacher extends UniversityEmployee {
    public subject: SubjectsList;

    constructor(name: string,position: Position, subject: SubjectsList){
        super(name,position);
        this.subject = subject;
    }

    gradeStudent(studentIndo:studentsInfo, studentName:string, regime: string): void {
        let scoreMessege = `${studentIndo.subject} teacher:`;
        
        if(studentIndo.preparationLevel < PrepLevels.Min) {
            if(regime === 'lesson') {
                studentIndo.score -= 10;
                scoreMessege += `student ${studentName} have bad level of preparation of and lower his/hers score to ${studentIndo.score}`;
            }else if(regime === 'transfer'){
                scoreMessege += `student ${studentName} did not prepare enough for transfer, have the same score ${studentIndo.score}`;
            }
        }else if((studentIndo.preparationLevel >= PrepLevels.Middle) && (studentIndo.preparationLevel < PrepLevels.Good)){ 
            if(regime === 'lesson'){
                studentIndo.score += 5;
                scoreMessege += `student ${studentName} have good level of preparation of and increases his/hers score to ${studentIndo.score}`;
            }else if(regime === 'transfer'){
                studentIndo.score += 20;
                scoreMessege += `student ${studentName} well prepared for transfer and increases his/hers score to ${studentIndo.score}`;
            }
        }
        else if(studentIndo.preparationLevel >= PrepLevels.Good){ 
            if(regime === 'lesson'){
                studentIndo.score += 10;
                scoreMessege += `student ${studentName} have excellent level of preparation of and increases his/hers score to ${studentIndo.score}`;
            }else if(regime === 'transfer') {
                studentIndo.score += 25;
                scoreMessege += `student ${studentName} excellent prepared for transfer and increases his/hers score to ${studentIndo.score}`;
            }
        }
        console.log(scoreMessege);
    }

    conductLesson(students: Student[]): void {
        for (const student of students) {
            let currStudent = student.studInfo;
            for (const subectsInfo of currStudent) {
                if(subectsInfo.subject === this.subject){
                    this.gradeStudent(subectsInfo,student.name,'lesson');
                }
            }
        }
    }
    
    receiveTransfer(student: Student,subectsInfo:studentsInfo): void {
        this.gradeStudent(subectsInfo,student.name,'transfer');
    }

}

class Librarian extends UniversityEmployee {
    constructor(name: string,position: Position){
        super(name,position);
    }

    helpStudent(student:any, studentsInfo:studentsInfo): void{
        studentsInfo.preparationLevel +=20;
        console.log(`librarian: student ${student.name} turned in libary to study ${studentsInfo.subject}, increases his/hers preparation to ${studentsInfo.preparationLevel}` )
    }
}

class HeadOfDepartment extends UniversityEmployee {
    constructor(name: string,position: Position){
        super(name,position);
    }

    calculateTeachersSalary(teachers: Teacher[], students: Student[]): void {
        let studentsAmount = students.length;
        console.log('Head Of Department:');
        for (const teacher of teachers) {
            let currentSubject = teacher.subject;
            let sumScrores: number = 0;
            for (const student of students) {
                let currentStudent = student.studInfo;
                for (const studInfo of currentStudent) {
                    if(studInfo.subject === currentSubject){ 
                        sumScrores += studInfo.score;
                    }
                }
            }
            let averageScore: number = Math.round(sumScrores/studentsAmount);
            if(averageScore < PrepLevels.Middle){
                teacher.Salary = TeachersSallaryLevels.Min;
                console.log(`teacher of  ${teacher.subject} have low average score on his subject and he recive low salary this mounth`);
            }else if((averageScore >= PrepLevels.Middle) && (averageScore < PrepLevels.Good)){
                teacher.Salary = TeachersSallaryLevels.Middle;
                console.log(`teacher of  ${teacher.subject} have good score on his subject and he recive middle salary this mounth`);
            }else if(averageScore >= PrepLevels.Good){
                teacher.Salary = TeachersSallaryLevels.Good;
                console.log(`teacher of  ${teacher.subject} have excellent score on his subject and he recive max salary this mounth`);
            }
        }
    }
}

class Dean extends UniversityEmployee {
    constructor(name: string,position: Position){
        super(name,position);
    }

    calculateScholarships(students: Student[]): void {
        for (const student of students) {
            let avaregeStudentScore: number = 0;
            let studentInfos = student.studInfo;
            let numberItems = studentInfos.length;
            for (const studentInfo of studentInfos) {
                avaregeStudentScore += studentInfo.score;
            }
            avaregeStudentScore = Math.round(avaregeStudentScore/numberItems);
            if((avaregeStudentScore >= PrepLevels.Middle) && (avaregeStudentScore < PrepLevels.Good)){
                student.scholarship = Scholarships.Normal;
                console.log(`Dean: student ${student.name} recive scholarship`);
            }else if(avaregeStudentScore >= PrepLevels.Good){
                student.scholarship = Scholarships.increased;
                console.log(`Dean: student ${student.name} recive increased scholarship`);
            }else {
                student.scholarship = false;
                console.log(`Dean: student ${student.name} is not reviving scholarship`);
            }
        }
    }
}

@studentDecorator
class Student implements StudentBase, StudentMethods {
    name: string;
    studInfo: studentsInfo[];
    @defaultValue(false)
    scholarship: ScholarshipLevels;

    constructor(name: string, studInfo: studentsInfo[],scholarship: ScholarshipLevels){
        this.name = name;
        this.studInfo = studInfo;
        this.scholarship = scholarship;
    }

    findTeacher(@creatingMethodLog teachers: Teacher[], subject: SubjectsList) {
        for (const teacher of teachers) {
            if(teacher.subject === subject){
                return teacher;
            }
        }
    }

    analysisScores(librarian: Librarian,teachers: Teacher[]): void {
        let myInfo = this.studInfo;
        for (const info of myInfo) {
            if(info.preparationLevel < PrepLevels.Min){
                librarian.helpStudent(this,info);
                let neededTecher = this.findTeacher(teachers, info.subject);
                neededTecher?.receiveTransfer(this,info);
            }
        }
    };
}

// Generics
class University<T>{
    constructor(public name: T, public students: Student[],public employees: employeesType){
        this.name = name;
        this.students = students;
        this.employees = employees;
    }

    @logUniversityMethod('Start of working process')
    workingPorcess(): void{
        this.teachersStartLessons();
        this.studentsStartWork();
        this.HeadOfDepartmentStartWork();
        this.deanStartWork();
    }

    @logUniversityMethod('Teachers starts lessons:')
    teachersStartLessons(): void{
        let teachers = this.employees.teachers; 
        for (const teacher of teachers) {
            teacher.conductLesson(this.students);
        }
    }

    @logUniversityMethod('Students and librarian start do their work:')
    studentsStartWork(): void {
        for (const student of this.students) {
            student.analysisScores(this.employees.librarian,this.employees.teachers);
        }
    }
    
    @logUniversityMethod('Head Of Department start do their work (calculate teachers salary):')
    HeadOfDepartmentStartWork(): void {
        this.employees.HeadOfDepartment.calculateTeachersSalary(this.employees.teachers,this.students);
    }

    @logUniversityMethod('Dean start to do their work (calculate students scholarships):')
    deanStartWork(): void {
        this.employees.Dean.calculateScholarships(this.students);
    }

}

// create new objects
let firstStudInfo: studentsInfo[] = [
    {subject: SubjectsList.Chemistry,
    preparationLevel: 60,
    score: 60},
    {subject: SubjectsList.Literature,
    preparationLevel: 80,
    score: 80},
    {subject: SubjectsList.Math,
    preparationLevel: 90,
    score: 90}
];

let secondStudInfo: studentsInfo[] = [
    {subject: SubjectsList.Chemistry,
    preparationLevel: 80,
    score: 80},
    {subject: SubjectsList.Literature,
    preparationLevel: 60,
    score: 60},
    {subject: SubjectsList.Math,
    preparationLevel: 90,
    score: 90}
];

let thirdStudInfo: studentsInfo[] = [
    {subject: SubjectsList.Chemistry,
    preparationLevel: 90,
    score: 90},
    {subject: SubjectsList.Literature,
    preparationLevel: 80,
    score: 80},
    {subject: SubjectsList.Math,
    preparationLevel: 40,
    score: 40}
]

let firstStudent = new Student('Andrew',firstStudInfo,false);
let secondStudent = new Student('John',secondStudInfo,false);
let thirdStudent = new Student('Elizabeth',thirdStudInfo,false);
let studArr: Student[] = [firstStudent,secondStudent,thirdStudent];

let teacherLiterature = new Teacher('Angela',Position.Teacher,SubjectsList.Literature);
let teacherChemistry = new Teacher('Walter',Position.Teacher,SubjectsList.Chemistry);
let teacherMath = new Teacher('Michael',Position.Teacher,SubjectsList.Math);
let teacherArr: Teacher[] = [teacherLiterature,teacherChemistry,teacherMath];

let librarian = new Librarian('Jim',Position.Librarian);
let NewHeadOfDepartment = new HeadOfDepartment('Hank',Position.HeadOfDepartment);
let newDean = new Dean('Stacy',Position.Dean);

let employeesList: employeesType = {
    teachers: teacherArr,
    librarian: librarian,
    HeadOfDepartment: NewHeadOfDepartment,
    Dean: newDean
};

let CHNUUniversity = new University<String>('CHNU',studArr,employeesList);
CHNUUniversity.workingPorcess();

console.log('------------------');
console.log(teacherLiterature.getSalary);
